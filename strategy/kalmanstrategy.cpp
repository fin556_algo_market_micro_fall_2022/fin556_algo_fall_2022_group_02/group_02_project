/*================================================================================
*     Source: ../RCM/StrategyStudio/examples/strategies/SimpleMomentumStrategy/SimpleMomentumStrategy.cpp
*     Last Update: 2013/6/1 13:55:14
*     Contents:
*     Distribution:
*
*
*     Copyright (c) RCM-X, 2011 - 2013.
*     All rights reserved.
*
*     This software is part of Licensed material, which is the property of RCM-X ("Company"),
*     and constitutes Confidential Information of the Company.
*     Unauthorized use, modification, duplication or distribution is strictly prohibited by Federal law.
*     No title to or ownership of this software is hereby transferred.
*
*     The software is provided "as is", and in no event shall the Company or any of its affiliates or successors be liable for any
*     damages, including any lost profits or other incidental or consequential damages relating to the use of this software.
*     The Company makes no representations or warranties, express or implied, with regards to this software.
/*================================================================================*/

#ifdef _WIN32
#include "stdafx.h"
#endif

#include "kalmanstrategy.h"


using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

KalmanStrategy::KalmanStrategy(StrategyID strategyID, const std::string& strategyName, const std::string& groupName):
        Strategy(strategyID, strategyName, groupName)
{
}

KalmanStrategy::~KalmanStrategy()
{
}

void KalmanStrategy::OnResetStrategyState()
{
}

void KalmanStrategy::DefineStrategyParams()
{
}

void KalmanStrategy::DefineStrategyCommands()
{
}

void KalmanStrategy::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate)
{

}
void KalmanStrategy::OnTrade(const TradeDataEventMsg& msg)
{
    auto smap_iter = smap_map.find(&msg.instrument());
    if (smap_iter == smap_map.end()) {
        smap_iter = smap_map.insert(std::make_pair(&msg.instrument(), Analytics::ScalarRollingWindow<double>(100))).first;
    }
    double smap = smap_iter->second.Mean();


    auto kalman_iter = kalman_map.find(&msg.instrument());
    if (kalman_iter == kalman_map.end()) {

        Eigen::MatrixXd A(n, n); // System dynamics matrix
        Eigen::MatrixXd C(m, n); // Output matrix
        Eigen::MatrixXd Q(n, n); // Process noise covariance
        Eigen::MatrixXd R(m, m); // Measurement noise covariance
        Eigen::MatrixXd P(n, n); // Estimate error covariance

        // Discrete LTI projectile motion, measuring position only
        A << 1, 0, 0, 1;
        C << 1, 0, 0, 1;
        Q << .09, .07, .05, .04;
        R << 2, 0, 0, 1;
        P << 10, 5, 2, 1;

        kalman_iter = kalman_map.insert(std::make_pair(&msg.instrument(), KalmanFilter(A, C, Q, R, P))).first;
        Eigen::VectorXd x0 = Eigen::VectorXd(2);
        x0 << msg.trade().price(), msg.trade().size();
        kalman_iter->second.init(x0);
    }
    else {
        KalmanFilter kf = kalman_iter->second;
        Eigen::VectorXd y(m);
        y << msg.trade().price(), msg.trade().size();
        kf.update(y);
        Eigen::VectorXd current_state = kf.state();
        if (smap < current_state[0]){
            if (portfolio().position(&msg.instrument()) < current_state[1]){
                SendBuyOrder(&msg.instrument(), current_state[1] - portfolio().position(&msg.instrument()));
            }
        }
        else if (smap > current_state[0]){
            if (portfolio().position(&msg.instrument()) > 0){
                SendSellOrder(&msg.instrument(), portfolio().position(&msg.instrument()));
            }
        }
    }
}

void KalmanStrategy::OnTopQuote(const QuoteEventMsg& msg)
{
}

void KalmanStrategy::SendBuyOrder(const Instrument* instrument, int quantity)
{
    OrderParams params(*instrument,
                       quantity,
                       (instrument->top_quote().ask() != 0) ? instrument->top_quote().ask() : instrument->last_trade().price(),
                       (instrument->type() == INSTRUMENT_TYPE_EQUITY) ? MARKET_CENTER_ID_NASDAQ : ((instrument->type() == INSTRUMENT_TYPE_OPTION) ? MARKET_CENTER_ID_CBOE_OPTIONS : MARKET_CENTER_ID_CME_GLOBEX),
                       ORDER_SIDE_BUY,
                       ORDER_TIF_DAY,
                       ORDER_TYPE_MARKET);

    trade_actions()->SendNewOrder(params);
}

void KalmanStrategy::SendSellOrder(const Instrument* instrument, int quantity)
{
    OrderParams params(*instrument,
                       quantity,
                       (instrument->top_quote().bid() != 0) ? instrument->top_quote().bid() : instrument->last_trade().price(),
                       (instrument->type() == INSTRUMENT_TYPE_EQUITY) ? MARKET_CENTER_ID_NASDAQ : ((instrument->type() == INSTRUMENT_TYPE_OPTION) ? MARKET_CENTER_ID_CBOE_OPTIONS : MARKET_CENTER_ID_CME_GLOBEX),
                       ORDER_SIDE_SELL,
                       ORDER_TIF_DAY,
                       ORDER_TYPE_MARKET);

    trade_actions()->SendNewOrder(params);
}

void KalmanStrategy::OnQuote(const QuoteEventMsg& msg)
{
}

void KalmanStrategy::OnDepth(const MarketDepthEventMsg& msg)
{
}

void KalmanStrategy::OnBar(const BarEventMsg& msg)
{
    std::cout << "BOOBS; " << std::endl;
}

void KalmanStrategy::OnOrderUpdate(const OrderUpdateEventMsg& msg)
{
}

void KalmanStrategy::OnStrategyCommand(const StrategyCommandEventMsg& msg)
{
}

void KalmanStrategy::OnParamChanged(StrategyParam& param)
{
}
